<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

// Definition of factory for categories
$factory->define(App\Category::class, function (Faker\Generator $faker){
    return [
        'denomination' => $faker->sentence(2, true)
    ];
});

// Definition of factory for transactions
$factory->define(App\Transaction::class, function (Faker\Generator $faker){
    return [
        'subject' => $faker->sentence(5,true),
        'amount' => $faker->randomFloat(2,1,30000),
        'type' => $faker->randomElement(array('withdrawal', 'deposit'))
        ];

});

