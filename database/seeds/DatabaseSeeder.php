<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\User::class, 1)->create([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'role' => 'admin',
        ]);

        //Factory for users
        factory(App\User::class, 5)->create([
            'role'=> 'user',
        ])->each(function(App\User $user){
            //Factory categories with user
            factory(App\Category::class)
                ->times(5)
                ->create([
                    'user_id' => $user->id,
                ])->each(function(App\Category $category){
                    //Factory of transactions with user and category
                    factory(App\Transaction::class)
                        ->times(5)
                        ->create([
                            'user_id' => random_int(2, 6),
                            'category_id' => $category->id,
                        ]);
                });
        });
    }
}
