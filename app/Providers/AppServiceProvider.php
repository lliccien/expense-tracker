<?php

namespace App\Providers;

use App\Repositories\CategoryRepository;
use App\Repositories\EloquentCategory;
use App\Repositories\EloquentTransaction;
use App\Repositories\TransactionRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CategoryRepository::class, EloquentCategory::class);
        $this->app->singleton(TransactionRepository::class, EloquentTransaction::class);
    }
}
