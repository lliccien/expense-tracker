<?php
/**
 * Created by PhpStorm.
 * User: Ludwring Liccien
 * Date: 10/8/2017
 * Time: 12:44 PM
 */

namespace App\Repositories;


use App\Category;

class EloquentCategory implements CategoryRepository
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentCategory constructor.
     *
     * @param App\Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * Get all categorys.
     */
    public function getAll()
    {
        $categories = $this->model->all();
        $data = [];

        foreach ($categories as $category){
            $item["id"]           = $category->id;
            $item["denomination"] = $category->denomination;
            $item["created_at"]   = $category->created_at->toDateTimeString();
            $item["updated_at"]   = $category->created_at->toDateTimeString();
            $item["username"]     = $category->user->name;
            $item["user_id"]      = $category->user_id;
            $data[]               = $item;
        }

        return response()->json($data,200);
    }

    /**
     * Get category by id.
     *
     * @param integer $id
     *
     * @return App\Category
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new category.
     *
     * @param array $attributes
     *
     * @return App\Category
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Update a category.
     *
     * @param integer $id
     * @param array $attributes
     *
     * @return App\Category
     */
    public function update($id, array $attributes)
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * Delete a category.
     *
     * @param integer $id
     *
     * @return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}