<?php
/**
 * Created by PhpStorm.
 * User: Ludwring Liccien
 * Date: 9/8/2017
 * Time: 9:38 PM
 */

namespace App\Repositories;


interface CategoryRepository
{
    public function getAll();

    public function getById($id);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function delete($id);
}