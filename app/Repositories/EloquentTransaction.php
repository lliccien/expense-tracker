<?php
/**
 * Created by PhpStorm.
 * User: Ludwring Liccien
 * Date: 10/8/2017
 * Time: 12:44 PM
 */

namespace App\Repositories;

use Carbon\Carbon;
use App\Transaction;

class EloquentTransaction implements TransactionRepository
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentTransaction constructor.
     *
     * @param App\Transaction $model
     */
    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }

    /**
     * Get all Transactions.
     */
    public function getAll()
    {
        $transactions = $this->model->all();
        $data = [];

        foreach ($transactions as $transaction){
            $item["id"]           = $transaction->id;
            $item["subject"]      = $transaction->subject;
            $item["amount"]       = $transaction->amount;
            $item["type"]         = $transaction->type;
            $item["category_id"]  = $transaction->category_id;
            $item["category"]     = $transaction->category->denomination;
            $item["created_at"]   = $transaction->created_at->toDateTimeString();
            $item["updated_at"]   = $transaction->created_at->toDateTimeString();
            $item["username"]     = $transaction->user->name;
            $item["user_id"]      = $transaction->user_id;
            $data[]               = $item;
        }

        return response()->json($data,200);
    }

    /**
     * Get Transaction by id.
     *
     * @param integer $id
     *
     * @return App\Transaction
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new Transaction.
     *
     * @param array $attributes
     *
     * @return App\Transaction
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Update a Transaction.
     *
     * @param integer $id
     * @param array $attributes
     *
     * @return App\Transaction
     */
    public function update($id, array $attributes)
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * Delete a Transaction.
     *
     * @param integer $id
     *
     * @return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    public function dayFilter($days, $user){
        $date = Carbon::now();
        $date->subDays($days);
        $transactions = Transaction::where('created_at', '>', $date->toDateTimeString()  )
            ->where('user_id', '=', $user)
            ->get();

        $data = [];
        foreach($transactions as $transaction){
            $item["id"]           = $transaction->id;
            $item["subject"]      = $transaction->subject;
            $item["amount"]       = $transaction->amount;
            $item["type"]         = $transaction->type;
            $item["category_id"]  = $transaction->category_id;
            $item["category"]     = $transaction->category->denomination;
            $item["created_at"]   = $transaction->created_at->toDateTimeString();
            $item["updated_at"]   = $transaction->created_at->toDateTimeString();
            $item["user_id"]      = $transaction->user_id;
            $data[] = $item;
         }

        $total = $this->average($data);
        array_unshift($data, $total);
        return response()->json($data);
    }
    /*
     * Function that generates the average of the transactions
     * Returns an array with the total deposit,  total withdrawal and balance
     * Return array
     */
    private function average($valor = array()){
        $totalDeposit = 0;
        $totalWithdrawal = 0;

        foreach ($valor as $value){
            if ($value['type'] == 'deposit'){
                $totalDeposit = $totalDeposit + $value['amount'];
            }else {
                $totalWithdrawal = $totalWithdrawal + $value['amount'];
            }
        }
        $totalDeposit = round($totalDeposit, 2);
        $totalWithdrawal = round($totalWithdrawal,2);

        $balance = $totalDeposit - $totalWithdrawal;

        $balance = round($balance,2);

        return ["total_deposit" => $totalDeposit, "total_withdrawal" => $totalWithdrawal, "balance" => $balance];
    }

}