<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $guarded = [];
    protected $fillable = [
        'subject',
        'amount',
        'type',
        'category_id',
        'user_id'
    ];

    // Relationship
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return$this->belongsTo(Category::class);
    }
}
