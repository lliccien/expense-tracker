<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryCreateRequest;

class CategoryController extends Controller
{
    protected $categories;

    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     * api/categories
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->categories->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {

        $attributes =[
            "denomination" => $request->input("denomination"),
            "user_id" => $request->input("user_id")
            //"user_id" => $request->user()->id
        ];

        $newCategory = $this->categories->create($attributes);

        return response()->json([
            'success' => true,
            'msg'=>'Category successfully created'],
            201);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category =  $this->categories->getById($id);
        return response()->json($category, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes =[
            "denomination" => $request->input("denomination"),
            "user_id" => $request->input("user_id")
        ];

        $updateCategory = $this->categories->update($id, $attributes);
        return response()->json([
            'success' => true,
            'msg'=>'Category successfully updated']
            , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteCategory = $this->categories->delete($id);
        return response()->json([
                'success' => true,
                'msg'=>'Category successfully deleted']
            , 200);
    }
}
