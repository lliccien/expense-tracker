<?php

namespace App\Http\Controllers;

use App\Repositories\TransactionRepository;
use App\Transaction;
use App\Http\Requests\TransactionCreateRequest;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    protected $transactions;

    public function __construct(TransactionRepository $trasactions)
    {
        $this->transactions = $trasactions;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->transactions->getAll();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionCreateRequest $request)
    {
        $attributes =[
            'subject' => $request->input("subject"),
            'amount' => $request->input("amount"),
            'type' => $request->input("type"),
            'category_id' => $request->input("category_id"),
            "user_id" => $request->input("user_id")
            //"user_id" => $request->user()->id
        ];

        $newTransaction = $this->transactions->create($attributes);

        return response()->json([
            'success' => true,
            'msg'=>'Transaction successfully created'],
            201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction =  $this->transactions->getById($id);
        return response()->json($transaction, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes =[
            'subject' => $request->input("subject"),
            'amount' => $request->input("amount"),
            'type' => $request->input("type"),
            'category_id' => $request->input("category_id"),
            "user_id" => $request->input("user_id")
        ];

        $updateTransaction = $this->transactions->update($id, $attributes);
        return response()->json([
                'success' => true,
                'msg'=>'Transaction successfully updated']
            , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteTransaction = $this->transactions->delete($id);
        return response()->json([
                'success' => true,
                'msg'=>'Transaction successfully deleted']
                , 200);
    }
    public function dayFilter($days, $user)
    {
        return $this->transactions->dayFilter($days, $user);
    }

}
