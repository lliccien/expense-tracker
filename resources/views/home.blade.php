@extends('layouts.app')

@section('content')

<div class="row">
    @if ( Auth::user()->role == 'admin' )
        <div class="col-md-8">
              <div class="card">
                  <div class="card-header bg-primary text-white">
                      Transactions
                  </div>
                  <div class="card-block">
              <showtransactions></showtransactions>
                  </div>
              </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Categories
                </div>
                <div class="card-block">
                <showcategories></showcategories>
                </div>
            </div>
        </div>
    @else
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Transactions
                </div>
                <div class="card-block">
                    <transactions :user="{{ Auth::user()->id }}"></transactions>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Categories
                </div>
                <div class="card-block">
                    <categories :user="{{ Auth::user()->id }}"></categories>
                </div>
            </div>
        </div>
    @endif
</div>

@endsection
